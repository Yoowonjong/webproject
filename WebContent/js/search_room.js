

$(document).ready(function()
{
	var regionSet = new Set();
	//현재 매물을 저장하는 Map (room_id(Key), 매물 정보 객체);
	var addressMap = new Map();

	
	//마커를 담고있는 배열
	//Global
	g_markers = [];


	var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
	geocoder = new daum.maps.services.Geocoder();
	var options = 
	{ //지도를 생성할 때 필요한 기본 옵션
		center: new daum.maps.LatLng(35.15743243890623, 126.87420456521055), //지도의 중심좌표.
		level: 3 //지도의 레벨(확대, 축소 정도)
	};


	//지도를 생성합니다.
	map = new daum.maps.Map(container, options);
	searchRoom();
	//지도의 중심좌표를 설정합니다.
	//map.setCenter(new daum.maps.LatLng(35.84045548525043, 127.11026624433109));


	
	
	//서버에서 데이터를 가져온다.
	//getItemListFromServer("광주", "서구", "내방동", addressMap);

	daum.maps.event.addListener(map, 'dragend', function() {
		//document.alert("이동");
		// 지도 중심좌표를 얻어옵니다
		var latlng = map.getCenter();

		var message = '변경된 지도 중심좌표는 ' + latlng.getLat() + ' 이고, ';
		message += '경도는 ' + latlng.getLng() + ' 입니다.';
		console.log(message);

		//지역의 검색 유무를 알아낼때 사용하는 Set
		searchRoom();
	});
	//Zoom 이벤트 핸들러를 설정합니다.
	daum.maps.event.addListener(map, 'zoom_changed', function()
	{
		searchRoom();
	});
	//마커에 mouseover 이벤트와 mouse out 이벤트를 등록
		var sido_index = -1;
		var sigungu_index_ = -1;
		var dong_index = -1;
		getAddressList();
		$('#sido-select').change(function(){
			//alert( $('#sido-select option:selected').val() );
			var sido = $('#sido-select option:selected').val();
			for(var i = 0; i < new_data.length; i++)
			{
				if(new_data[i].name == sido)
				{
					sido_index = i;
					//옆에 sigungu option을 모두 지운다.
					$("select[id='sigungu-select']").find('option').each(function() {
					   $(this).remove();
					 });
					
					//alert(new_data[i].items.length);
					for(var k = 0; k < new_data[i].items.length; k++)
					{
						var addOpt = document.createElement("option");
						addOpt.value = new_data[i].items[k].name;
						addOpt.appendChild(document.createTextNode(new_data[i].items[k].name));
						$("#sigungu-select").append(addOpt);
					}
				}
			}
		});
		
		$('#sigungu-select').change(function(){
			//alert( $('#sigungu-select option:selected').val() );
			var sido = $('#sido-select option:selected').val();
			var sigungu = $('#sigungu-select option:selected').val();
			for(var i = 0; i < new_data.length; i++)
			{
				if(new_data[i].name == sido)
				{
					for(var k = 0; k < new_data[i].items.length; k++)
					{
						if(new_data[i].items[k].name == sigungu)
						{
							sigungu_index_ = q;
							$("select[id='dong-select']").find('option').each(function() {
							   $(this).remove();
							 });
							for(var q = 0; q < new_data[i].items[k].items.length; q++)
							{
								var addOpt = document.createElement("option");
								addOpt.value = new_data[i].items[k].items[q].name;
								addOpt.appendChild(document.createTextNode(new_data[i].items[k].items[q].name));
								$("#dong-select").append(addOpt);
							}
						}
					}
				}
			}
		});
		$('#dong-select').change(function(){
			var dong_name = $('#dong-select option:selected').val();
			
			var sido = $('#sido-select option:selected').val();
			var sigungu = $('#sigungu-select option:selected').val();
			for(var i = 0; i < new_data.length; i++)
			{
				if(new_data[i].name == sido)
				{
					for(var k = 0; k < new_data[i].items.length; k++)
					{
						if(new_data[i].items[k].name == sigungu)
						{
							for(var q = 0; q < new_data[i].items[k].items.length; q++)
							{
								if(new_data[i].items[k].items[q].name == dong_name)
								{
									var moveLatLng = new daum.maps.LatLng(new_data[i].items[k].items[q].lat,
											new_data[i].items[k].items[q].lng);
									map.setCenter(moveLatLng);
									searchRoom();
									break;
								}
							}
						}
					}
				}
			}

		});

    //----------------------------------------------------------
    $('.detail-option-container').hide(100);
    //상세검색 엘리먼트가 hide인지 show인지 나타내는 flag
    is_open_detail = false;
    //상세검색 버튼 클릭시 이벤트 처리
    $('#down-button').click(function() {
        if(is_open_detail)
        {
            $('.detail-option-container').hide(1000);
            is_open_detail = false;
        }
        else
        {
            $('.detail-option-container').show(1000);
            is_open_detail = true;
        }
    });
    //----------------------------------------------------------

    //----------------------------------------------------------
    //input 엘리먼트 값 바뀌었을 때 호출되는 함수.
    $(document).on('keyup','input[type="text"]',function(e){
        var $this = $(this);

        if($this.val() !== $this.data('previousValue')){
            $this.trigger('change');
        }
        $this.data('previousValue',$this.val());

    });
    /*$('input[type="text"]').on('change',function(e){
        console.log('changed',this.value, e);
        alert(e);
        a = e;
    });*/
    $('#deposit-min').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });
    $('#deposit-max').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });
    $('#monthly-rent-min').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });
    $('#monthly-rent-max').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });
    $('#room-size-min').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });
    $('#room-size-max').on('change' ,function(e) {
        this.value = this.value.replace(/[,\-A-Za-z]/g, "");
        searchRoom();
    });


    //체크박스 클릭시 이벤트 핸들러 등록
    $('input[type=checkbox]').click(function() {
         searchRoom();
    });
});


function searchAddrFromCoords(coords, callback) {
    // 좌표로 행정동 주소 정보를 요청합니다
    geocoder.coord2addr(coords, callback);         
}

function searchDetailAddrFromCoords(coords, callback) {
    // 좌표로 법정동 상세 주소 정보를 요청합니다
    geocoder.coord2detailaddr(coords, callback);
}

// 지도 좌측상단에 지도 중심좌표에 대한 주소정보를 표출하는 함수입니다
function displayCenterInfo(status, result) {
    if (status === daum.maps.services.Status.OK) {
        var infoDiv = document.getElementById('centerAddr');
        infoDiv.innerHTML = result[0].fullName;
    }    
}

function searchRoom()
{
	var bound = map.getBounds();
	var northEast = bound.getNorthEast();
	var southWest = bound.getSouthWest();
	var start_x = southWest.getLat();
	var start_y = southWest.getLng();
	var end_x = northEast.getLat();
	var end_y = northEast.getLng();
    //추가 구현사항
    //상세보기도 추가하기 ㅠㅠ
    var deposit_min = $('#deposit-min').val();
    var deposit_max = $('#deposit-max').val();

    var monthly_rent_min = $('#monthly-rent-min').val();
    var monthly_rent_max = $('#monthly-rent-max').val();

    var room_size_min = $('#room-size-min').val();
    var room_size_max = $('#room-size-max').val();

    //방 구조 배열만들어서 넣기
    var room_structure = [];
    $('.item_room').each(function() {
        if(this.checked)
        {
            room_structure.push(this.value);
        }
    })
	$.ajax({
		url : "api/search_room_position.jsp",
		type : "POST",
		data : JSON.stringify({
			"start_x" : start_x,
			"start_y" : start_y,
			"end_x" : end_x,
			"end_y" : end_y,
            "deposit_min" : deposit_min,
            "deposit_max" : deposit_max,
            "monthly_rent_min" : monthly_rent_min,
            "monthly_rent_max" : monthly_rent_max,
            "room_size_min" : room_size_min,
            "room_size_max" : room_size_max,
            "room_structure" : room_structure
		}),
		dataType : "json",
		success : function(data) {
			var result = data.result;

			if(result == "true")
			{
				var list = data.room_list;
				//모든 매물들을 삭제합니다.
				for(var i = 0; i < g_markers.length; i++)
				{
					g_markers[i].setMap(null);
				}
				g_markers.length = 0;
				$(".list-group-item").each(function() {
					$(this).remove();
				});
				for(var i = 0; i < list.length; i++)
				{
					var room_id = list[i].room_id;
					var security_deposit = list[i].security_deposit;
					var monthly_rent = list[i].monthlyRent;
					var room_structure = list[i].room_structure;
					var title = list[i].title;
					var latitude = list[i].latitude;
					var longitude = list[i].longitude;
					var picture_path = list[i].picture_path;



					var html = "<a class='list-group-item' href='item_detail.jsp?room_id=" + room_id+"' id='" + room_id + "' class='item-container'>" +
						"<div class='row'>" +
						"<div style='padding-left:0px; padding-right:0px;'class='col-lg-3 col-sm-3 col-md-3 image-container'><img class='item-image' src='" + picture_path  + "'></div>" +
						"<div class='col-lg-9 col-sm-9 col-md-9 info-container'>" +
						"<h4 class='list-group-item-heading'>" + security_deposit + "/" + monthly_rent + "</h4>" +
						"<p class='list-group-item-text'>" + room_structure + "</p>" +
						"<p class='list-group-item-text'>" + title + "</p>" +
						"</div>" +
						"</div>" +
						"</a>";
					$('#item-list-container').append(html);
					var nowMarkerPosition = new daum.maps.LatLng(latitude, longitude);
					console.log("lat : " + latitude + "lng : " + longitude);
					var marker = new daum.maps.Marker({
						map:map,
						position: nowMarkerPosition,
						title: room_id
					});
					g_markers.push(marker);
					var infowindow = new daum.maps.InfoWindow({
						content : "<div style='width:200px;'><a class='list-group-item' href='item_detail.jsp?room_id=" + room_id+"' id='" + room_id + "' class='item-container'>" +
								"<div class='row'>" +
								"<div style='padding-left:0px; padding-right:0px;'class='col-lg-3 col-sm-3 col-md-3 image-container'><img class='item-image' src='" + picture_path  + "'></div>" +
								"<div class='col-lg-9 col-sm-9 col-md-9 info-container'>" +
								"<h4 class='list-group-item-heading'>" + security_deposit + "/" + monthly_rent + "</h4>" +
								"<p class='list-group-item-text'>" + room_structure + "</p>" +
								"<p class='list-group-item-text'>" + title + "</p>" +
								"</div>" +
								"</div>" +
								"</a></div>"
					});

					// 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
					// 이벤트 리스너로는 클로저를 만들어 등록합니다
					// for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
					daum.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
					daum.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));

				}
			}
			else
			{
				alert("서버 오류!");
			}
		}
		,
		fail : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		},
		error : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		}
	});

}


function getAddressList()
{
	$.ajax({
		url : "api/getAddress.jsp",
		type : "POST",
		dataType : "json",
		success : function(data) {
			new_data = [];
			//alert('성공!');
        	for (var i = 0; i < data.length; i++) {
        		var address = data[i];
        		if (address.name == "서울시") {
        			new_data[0] = address;
        		} else if (address.name == "경기도") {
        			new_data[1] = address;
        		} else if (address.name == "인천시") {
        			new_data[2] = address;
        		} else if (address.name == "부산시") {
        			new_data[3] = address;
        		} else if (address.name == "대구시") {
        			new_data[4] = address;
        		} else if (address.name == "대전시") {
        			new_data[5] = address;
        		} else if (address.name == "울산시") {
        			new_data[6] = address;
        		} else if (address.name == "세종시") {
        			new_data[7] = address;
        		} else if (address.name == "광주시") {
        			new_data[8] = address;
        		} else if (address.name == "강원도") {
        			new_data[9] = address;
        		} else if (address.name == "충청북도") {
        			new_data[10] = address;
        		} else if (address.name == "충청남도") {
        			new_data[11] = address;
        		} else if (address.name == "경상북도") {
        			new_data[12] = address;
        		} else if (address.name == "경상남도") {
        			new_data[13] = address;
        		} else if (address.name == "전라북도") {
        			new_data[14] = address;
        		} else if (address.name == "전라남도") {
        			new_data[15] = address;
        		} else if (address.name == "제주도") {
        			new_data[16] = address;
        		}
        	}
		},
		fail : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		},
		error : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		}
	});
}


/*function detailAddressToPosition(sido, sigungu, dong, detail, addressMap, room_id)
{
	var detailAddress = sido + " " + sigungu + " " + dong + " " + detail;
	//해당 위치로 상세 주소를 얻어온다.
	geocoder.addr2coord(detailAddress, function(status, result) {
		if(status === daum.maps.services.Status.OK)
		{
			var coords = new daum.map.LatLng(result.addr[0].lat, result.addr[0].lng);

			//해당 좌표를 서버에 전송한다.
			//하고 결과를 가져온다.

			$.ajax({
				url : "api/saveRoomPosition.jsp",
				type : "POST",
				data : JSON.stringify({ "room_id" : "0",
					"lat" : "-23424.2444",
					"lng" : "-223.2323"}),
				dataType : "json",
				success : function(data) {
					if(data.result === true)
					{
						//Todo: 데이터를 어떻게 처리할까?
					}
					else
					{
						alert("detailAddressToPosition 실패 : " + data.reason);
					}

				},
				fail : function(e) {
					alert("데이터 요청에 실패하였습니다.\r status : " + e);
				},
				error : function(e) {
					alert("데이터 요청에 실패하였습니다.\r status : " + e);
				}
			});


		}
	});
}*/


function addMarkerInMap(addressMap, room_id)
{

}


function getItemDetailInfoFromServer(room_id_array)
{
	console.log(room_id_array);
	$.ajax({
		url : "api/searchRoomShortInfo.jsp",
		type : "POST",
		data : JSON.stringify({
			"room_id" : room_id_array
		}),
		dataType : "json",
		success : function(data) {
			var roomList = data.room_list;
			/*
				 info.put("room_id", roomIDArray.get(i));
				 info.put("security_deposit", securityDeposit);
				 info.put("monthlyRent", monthlyRent);
				 info.put("room_structure", roomStructure);
				 info.put("title", title);
				 info.put("picture_path", p);
			 */
			for(var i = 0; i < roomList.length; i++)
			{
				var html = "<a class='list-group-item' href='#' id='" + roomList[i].room_id + "' class='item-container'>" +
					"<div class='row'>" +
					"<div style='padding-left:0px; padding-right:0px;'class='col-lg-3 col-sm-3 col-md-3 image-container'><img class='item-image' src='" + roomList[i].picture_path  + "'></div>" +
					"<div class='col-lg-9 col-sm-9 col-md-9 info-container'>" +
					"<h4 class='list-group-item-heading'>" + roomList[i].security_deposit + "/" + roomList[i].monthlyRent + "</h4>" +
					"<p class='list-group-item-text'>" + roomList[i].room_structure + "</p>" +
					"<p class='list-group-item-text'>" + roomList[i].title + "</p>" +
					"</div>" +
					"</div>" +
					"</a>";
				$('#item-list-container').append(html);
			}





		}
		,
		fail : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		},
		error : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		}
	});
}


//동을 받아서 데이터베이스 Look Up
//Map(Data Structure)에 존재 유무 검사, Map에 추가
function getItemListFromServer(sido, sigungu, dongName, addressMap)
{
	//비동기

	$.ajax({
		url : "api/searchRoomList.jsp",
		type : "POST",
		data : JSON.stringify({ "sido" : sido,
			"sigungu" : sigungu,
			"dong" : dongName}),
		dataType : "json",
		success : function(data) {
			/*
				[data :
				room_id : int
				sido : string
				sigungu : string
				jibun_address : String
				road_address : String
				
				
				latitude : 위도
				longitude : 경도
				] : room_list (JSON Array)


				result : 결과값 저장 (true, false)
				reason : 이유
			 */

			if(data.result === "true") {
				var room_list = data.room_list;
				//받은 방의 정보들을 순회한다.
				var roomIDArray = [];
				room_list.forEach(function(item) {
					//아니면 해당 room_id로 Map을 탐색하고 없으면 Map에 추가 및 지도에 마커를 추가한다.

					roomIDArray.push(item.room_id);

					addressMap.set(item.room_id, {
						sido : item.sido,
						sigungu : item.gigungu,
						dong : item.dong,
						jibun_address : item.jibun_address,
						road_address : item.road_address,
						latitude : item.latitude,
						longitude : item.longitude
					});

					var nowMarkerPosition = new daum.maps.LatLng(item.latitude, item.longitude);
					console.log("lat : " + item.latitude + "lng : " + item.longitude);
					var marker = new daum.maps.Marker({
						map:map,
						position: nowMarkerPosition,
						title: item.room_id
					});

					var infowindow = new daum.maps.InfoWindow({
						content : '<div>마이크 테스트</div>'
					});

					// 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
					// 이벤트 리스너로는 클로저를 만들어 등록합니다
					// for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
					daum.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
					daum.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
					
				});
				getItemDetailInfoFromServer(roomIDArray);

			}
			else {
				alert("getItemListFromServer 실패!")
			}
		},
		fail : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		},
		error : function(e) {
			alert("데이터 요청에 실패하였습니다.\r status : " + e);
		}
	});
}


//daum.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
//mouseover 이벤트 Listener이다.
function makeOverListener(map, marker, infowindow) {
	return function() {
		infowindow.open(map, marker);
	};
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다
function makeOutListener(infowindow) {
	return function() {
		infowindow.close();
	};
}


//왼쪽 모서리와 오른쪽 아래 모서리 좌표를 받아서
//25군데의 주소를 탐색한다.
//만약 존재하지 않는 지역이라면 서버에서 검색해서 가져온다.
function getDongName(regionSet,left, top, right, bottom)
{
	//동 이름을 구한다.
	var width_diff = right - left;
	var height_diff = top - bottom;
	
	for(var i = 0; i < 5; i++)
	{
		for(var k = 0; k < 5; k++)
		{
			geocoder.coord2detailaddr(new daum.maps.LatLng(left + width_diff * i, top + height_diff * k), function(status, result) {
				if(status === daum.maps.services.Status.OK)
				{
					var region = result[0].region;
					//만약 해당 지역이 이미 검색 되지 않았다면
					if(!regionSet.has(region))
					{
						regionSet.add(region);
						//해당 동 이름으로 검색
						console.log("동 이름 : " + region);
					}
					else //아니면 Skip
					{

					}


				}

			});

		}
	}
	
	
}