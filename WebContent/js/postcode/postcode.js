



//sido 			: 도/시 이름
//sigungu 		: 시/군/구 이름
//bname 		: 동 이름
//jibunAddress 	: 지번 주소(동)
//roadAddress 	: 도로명 주소

function daumAPI()
{
	//주소를 검색한다.
	new daum.Postcode( {
		oncomplete:function(data) {
			var geocoder = new daum.maps.services.Geocoder();
			//주소로 좌표를 가져온다.
			geocoder.addr2coord(data.roadAddress, function(status, result) {
				if(status === daum.maps.services.Status.OK)
				{
					//input 엘리먼트에 각 요소들을 채운다!!>
					$('#address-number').val(data.postcode);
					$('#jibun-address').val(data.jibunAddress);
					$('#lat').val(result.addr[0].lat);
					$('#lng').val(result.addr[0].lng);
					$('#sido').val(data.sido);
					$('#sigungu').val(data.sigungu);
					$('#dong').val(data.bname);
					$('#road-address').val(data.roadAddress);
				}
				else
				{
					alert("주소 검색 실패 다시 시도하세요.");
				}
			});
		}
	}).open();
}

$(document).ready(function() {
	/*new daum.Postcode( {
		oncomplete:function(data) {
			var geocoder = new daum.maps.services.Geocoder();
			//주소로 좌표를 가져온다.
			alert(data.jibunAddress);
			alert(data.autoJibunAddress);
			geocoder.addr2coord(data.roadAddress, function(status, result) {
				if(status === daum.maps.services.Status.OK)
				{
					$.ajax({
						url : "api/addAddress.jsp",
						type : "POST",
						data : JSON.stringify({ 
							"sido" : data.sido,
							"sigungu" : data.sigungu,
							"dong" : data.bname,
							"jibun_address" : data.jibunAddress,
							"road_address" : data.roadAddress,
							"latitude" : result.addr[0].lat, 
							"longitude" : result.addr[0].lng}),
						dataType : "json",
						success : function(data) {
							if(data.result === "true") {
								alert("성공!")
							}
							else {
								alert("getItemListFromServer 실패!")
							}
								
						},
						fail : function(e) {
							alert("데이터 요청에 실패하였습니다.\r status : " + e);
						},
						error : function(e) {
							alert("데이터 요청에 실패하였습니다.\r status : " + e);
						}
					});
				}
				else
				{
					alert("실패");
				}	
			});
		}
	}).open();*/

});