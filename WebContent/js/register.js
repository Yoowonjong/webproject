var register_type = 0;
	

function setRegisterType(type)
{
	register_type = type;
	for(var i = 1; i <= 3; i++)
	{
		//선택되지 않은 거에 대해서
		if(i == type) 
		{
			$('#'+i).css("background-color", "#59ab02");
			$('#'+i).css("color", "#ffffff");
		}
		else
		{
			$('#'+i).css("background-color", "#ffffff");
			$('#'+i).css("color", "#000000");
		}
	}
	
	
		$('#len').val(type);
	
	//부동산 업자일 경우 컬럼 추가

	if(type == 2)
	{
		$('.register-agent-list').remove();
		var html = "" +
			"<div class='register-agent-list'>" +
			"	<div class='register-agent form-group-lg'>" +
			"		<div class='row'>" +
			"			<div class='col-md-12'>" +
			"				<input id='agent_name' name='agent_name' type='text' value='' class='form-control' placeholder='부동산 이름' required>" +
			"			</div>" +
			"			<div class='col-md-12'>" +
			"				<input id='agent_position' name='agent_position' type='text' value='' class='form-control' placeholder='위치' required>" +
			"			</div>" +
			"			<div class='col-md-12'>" +
			"				<input id='agent_telephone' name='agent_telephone' type='text' value='' class='form-control' placeholder='대표 번호' >" +
			"			</div>" +
			"		</div>" +
			"	</div>" +
			"</div>"
		$('.modal-body').append(html);
	}

	else
	{
		$('.register-agent-list').remove();
	}
}

$(document).ready(function() {
	$('#register-button').click(function() {
		var email = $('#register-email').val();
		if(email == '')
		{
			$('#register-email').css('background-color','red');
			$('#alert-message').show();
		}

	});
	$('#alert-message').hide();
})