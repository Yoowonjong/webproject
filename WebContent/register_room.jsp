<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-theme.css">
<title>Insert title here</title>
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	<section id="main">
		<div class="main-content">
			<div class="container">
				<form name="register_room" enctype="multpart/form-data" action="register_process.jsp" method="post">
				   <input type="file" name="uploadFile" id="uploadFile"> 
				   <select name="picture_type">
						<option value="1">VR</option>
						<option value="2">일반사진</option>
					</select><br>
					<label>보증금</label>
					<input name="security_deposit" type="text" required>만원 <br>
					<label>월세</label>
					<input name="monthly_rent" type="text" required>만원<br>
					<label>방구조</label>
					<select name="room_structure">
						<option value="원룸(오픈형)">원룸(오픈형)</option>
						<option value="원룸(분리형)">원룸(분리형)</option>
						<option value="원룸(복층형)">원룸(복층형)</option>
						<option value="투룸">투룸</option>
						<option value="쓰리룸">쓰리룸</option>
					</select><br>
					<label>관리비</label>
					<input name="management_expenses" type="text" required>만원<br>
					<input type="checkbox" name="management_list" value="전기세">전기세
					<input type="checkbox" name="management_list" value="가스">가스
					<input type="checkbox" name="management_list" value="수도">수도
					<input type="checkbox" name="management_list" value="인터넷">인터넷
					<input type="checkbox" name="management_list" value="TV">TV<br>
					<label>크기 전용면적 ㅣ:</label>
					<input type="text" name="area_for_exclusive_use" required>m^2<br>
					<label>크기 계약면적 : </label>
					<input type="text" name="area_for_agreement" required>m^2<br>
					<label>층 수</label>
					<input  name="number_of_building_story" type="text" required> / 해당 층 <input  name="number_of_story" type="text" required>
					<select name="building_direction">
                            <option value="동향">동향</option>
                            <option value="서향">서향</option>
                            <option value="남향">남향</option>
                            <option value="북향">북향</option>
                            <option value="남동향">남동향</option>
                            <option value="남서향">남서향</option>
                            <option value="북동향">북동향</option>
                            <option value="북서향">북서향</option>
                            <option value="확인필요">확인필요</option>
                    </select>
                    <div class="i-options">
                        <label><input type="checkbox" name="room_options" value="에어컨"> 에어컨</label>
                        <label><input type="checkbox" name="room_options" value="냉장고"> 냉장고</label>
                        <label><input type="checkbox" name="room_options" value="세탁기"> 세탁기</label>
                        <label><input type="checkbox" name="room_options" value="가스레인지"> 가스레인지</label>
                        <label><input type="checkbox" name="room_options" value="인덕션"> 인덕션</label>
                        <label><input type="checkbox" name="room_options" value="전자레인지"> 전자레인지</label>
                        <label><input type="checkbox" name="room_options" value="책상"> 책상</label>
                        <label><input type="checkbox" name="room_options" value="책장"> 책장</label>
                        <label><input type="checkbox" name="room_options" value="침대"> 침대</label>
                        <label><input type="checkbox" name="room_options" value="옷장"> 옷장</label>
                        <label><input type="checkbox" name="room_options" value="신발장"> 신발장</label>
                        <label><input type="checkbox" name="room_options" value="싱크대"> 싱크대</label>
                    </div>
                    <div>
                        <label><input type="radio" name="is_available_loan" value="가능" checked> 가능 </label>
                        <label><input type="radio" name="is_available_loan" value="불가능"> 불가능 </label>
                    </div>
                    <div>
                        <label><input type="radio" name="is_available_animal" value="가능" checked> 가능</label>
                        <label><input type="radio" name="is_available_animal" value="불가능"> 불가능</label>
                    </div>
                    <div class="has-col">
                        <label><input type="radio" name="is_available_parking" value="가능" checked> 가능</label>
                        <label><input type="radio" name="is_available_parking" value="없음"> 없음</label>
    
                    </div>
                    <div class="i-col">
                            <strong>엘리베이터</strong>
                            <label><input type="radio" name="is_available_elevator" value="있음" checked> 있음</label>
                            <label><input type="radio" name="is_available_elevator" value="없음"> 없음</label>
                    </div>
					<div class="address-form-group">
						<!-- Hidden Input -->
						<input type="hidden" id="lat" name="lat">
						<input type="hidden" id="lng" name="lng">
						<input type="hidden" id="sido" name="sido">
						<input type="hidden" id="sigungu" name="sigungu">
						<input type="hidden" id="dong" name="dong">
						<input type="hidden" id="road-address" name="road-address">
						<label for="InputEmail">주소</label>
						
						<input type="text" class="form-control" id="address-number" name="address-number" placeholder="우편번호" readonly="readonly">
						<button type="button" class="btn btn-default" onclick="daumAPI()">주소찾기</button>
						
						<input type="text" class="form-control" id="jibun-address" name="jibun-address" placeholder="주소" readonly="readonly">
						<input type="text" class="form-control" id="remainder_address" name="remainder_address" placeholder="나머지 주소" required>
					
					</div>
					
					<div class="submit_button">
						<input type="submit">
					</div>
			
				</form>
			</div>
		</div>
	</section>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script type="text/javascript" src="js/postcode/postcode.js"></script>
<script type="text/javascript" charset='UTF-8' src="//apis.daum.net/maps/maps3.js?apikey=9091c7b57e5b32c1d21d9e0647a07843&libraries=services,clusterer"></script>

</html>