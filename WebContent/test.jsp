<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	<section id="main">
		<div class="main-content">
			<div class="container">
				<form>
					<label>보증금</label>
					<input id="security_deposit" type="text">만원 <br>
					<label>월세</label>
					<input id="monthly_rent" type="text">만원<br>
					<label>방구조</label>
					<select>
						<option>원룸(오픈형)</option>
						<option>원룸(분리형)</option>
						<option>원룸(복층형)</option>
						<option>투룸</option>
						<option>쓰리룸</option>
					</select><br>
					<label>관리비</label>
					<input id="management_expenses" type="text">만원<br>
					<input type="checkbox" name="management_list" value="전기세">전기세
					<input type="checkbox" name="management_list" value="가스">가스
					<input type="checkbox" name="management_list" value="수도">수도
					<input type="checkbox" name="management_list" value="인터넷">인터넷
					<input type="checkbox" name="management_list" value="TV">TV<br>
					<label>크기 전용면적 ㅣ:</label>
					<input type="text" id="area_for_exclusive_use">m^2<br>
					<label>크기 계약면적 : </label>
					<input type="text" id="area_for_agreement">m^2<br>
					<label>층 수</label>
					<input type="text" id="number_of_building_story"> / 해당 층 <input type="text" id="number_of_story">
					<select name="room_direction">
                            <option value="">선택하세요</option>
                            <option value="E">동향</option>
                            <option value="W">서향</option>
                            <option value="S">남향</option>
                            <option value="N">북향</option>
                            <option value="SE">남동향</option>
                            <option value="SW">남서향</option>
                            <option value="NE">북동향</option>
                            <option value="NW">북서향</option>
                            <option value="-">확인필요</option>
                    </select>
                    <div class="i-options">
                        <label><input type="checkbox" name="room_options" value="01"> 에어컨</label>
                        <label><input type="checkbox" name="room_options" value="02"> 냉장고</label>
                        <label><input type="checkbox" name="room_options" value="03"> 세탁기</label>
                        <label><input type="checkbox" name="room_options" value="04"> 가스레인지</label>
                        <label><input type="checkbox" name="room_options" value="05"> 인덕션</label>
                        <label><input type="checkbox" name="room_options" value="06"> 전자레인지</label>
                        <label><input type="checkbox" name="room_options" value="07"> 책상</label>
                        <label><input type="checkbox" name="room_options" value="08"> 책장</label>
                        <label><input type="checkbox" name="room_options" value="09"> 침대</label>
                        <label><input type="checkbox" name="room_options" value="10"> 옷장</label>
                        <label><input type="checkbox" name="room_options" value="11"> 신발장</label>
                        <label><input type="checkbox" name="room_options" value="12"> 싱크대</label>
                    </div>
                    <div>
                        <label><input type="radio" name="pets" value="yes"> 가능</label>
                        <label><input type="radio" name="pets" value="no"> 불가능</label>
                        <label><input type="radio" name="pets" value="catonly"> 고양이만</label>
                        <label><input type="radio" name="pets" value="-"> 확인필요</label>
                    </div>
                    <div class="has-col">
                        <label><input type="radio" name="parking" value="가능"> 가능</label>
                        <label><input type="radio" name="parking" value="불가능"> 없음</label>
    
                    </div>
                    <div class="i-col">
                            <strong>엘리베이터</strong>
                            <label><input type="radio" name="building_elevator" value="true"> 있음</label>
                            <label><input type="radio" name="building_elevator" value="false"> 없음</label>
                    </div>
					<div class="address-form-group">
						<!-- Hidden Input -->
						<input type="hidden" id="lat" name="lat">
						<input type="hidden" id="lng" name="lng">
						<input type="hidden" id="sido" name="sido">
						<input type="hidden" id="sigungu" name="sigungu">
						<input type="hidden" id="dong" name="dong">
						<input type="hidden" id="road-address" name="jibun_address">
						<label for="InputEmail">주소</label>
						
						<input type="text" class="form-control" id="address-number" placeholder="우편번호" readonly="readonly">
						<button type="button" class="btn btn-default" onclick="daumAPI()">주소찾기</button>
						
						<input type="text" class="form-control" id="jibun-address" placeholder="주소" readonly="readonly">
						<input type="text" class="form-control" id="address-number" placeholder="나머지 주소">
					
					</div>
					
					
			
				</form>
			</div>
		</div>
	</section>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script type="text/javascript" src="js/postcode/postcode.js"></script>
<script type="text/javascript" charset='UTF-8' src="//apis.daum.net/maps/maps3.js?apikey=9091c7b57e5b32c1d21d9e0647a07843&libraries=services,clusterer"></script>

</html>