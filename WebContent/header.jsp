<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
        <title>Home | Gallaxy Responsive HTML5/CSS3 Template | FIFO THEMES</title>
        <meta name="description" content="Gallaxy Responsive HTML5/CSS3 Template from FIFOTHEMES.COM">
        <meta name="author" content="FIFOTHEMES.COM">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>
        <!-- Library CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="css/animations.css" media="screen">
        <link rel="stylesheet" href="css/superfish.css" media="screen">
        <link rel="stylesheet" href="css/revolution-slider/css/settings.css" media="screen">
        <link rel="stylesheet" href="css/revolution-slider/css/extralayers.css" media="screen">
        <link rel="stylesheet" href="css/prettyPhoto.css" media="screen">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Skin -->
        <link rel="stylesheet" href="css/colors/green.css" class="colors">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/theme-responsive.css">
        <!-- Switcher CSS -->
        <link href="css/switcher.css" rel="stylesheet">
        <link href="css/spectrum.css" rel="stylesheet">
        <!-- Favicons -->
        <link rel="shortcut icon" href="img/ico/favicon.ico">
        <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144.png">
<div class="page-mask">
            <div class="page-loader">
                <div class="spinner"></div>
                Loading...
            </div>
        </div>
        <!-- Wrap -->
        <div class="wrap">
            <!-- Header -->
            <header id="header">
                <!-- Header Top Bar -->
                <div class="top-bar">
                    <div class="slidedown collapse">
                        <div class="container">
                            <div class="phone-login pull-right">
                                <%
                                if(session.getAttribute("email") == null)
                                {
                                %>
                                	<a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in"></i> 로그인</a>
                                	<a href="#" data-toggle="modal" data-target="#registrationModal"><i class="fa fa-edit"></i> 회원가입</a>
                                <%
                                } 
                                else
                                {
                                	
                                %>
                                	<span class="label label-default"><%=session.getAttribute("email")%>님 환영합니다.</span>
                                	<a href="logout_process.jsp"><i class="fa fa-sign-in"></i> 로그아웃</a>
                                <%
                                }
                                %>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Header Top Bar -->
                <!-- Main Header -->
                <div class="main-header">
                    <div class="container">
                        <!-- TopNav -->
                        <div class="topnav navbar-header">
                            <a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
                            <i class="fa fa-angle-down icon-current"></i>
                            </a> 
                        </div>
                        <!-- /TopNav-->
                        <!-- Logo -->
                        <div class="logo pull-left">
                            <h1>
                                <a href="index.jsp">
                                <img class="logo-color" src="logo.jpg" alt="To You" width="160" height="60">
                                </a>
                            </h1>
                        </div>
                        <!-- /Logo -->
                        <!-- Mobile Menu -->
                        <div class="mobile navbar-header">
                            <a class="navbar-toggle" data-toggle="collapse" href=".navbar-collapse">
                            <i class="fa fa-bars fa-2x"></i>
                            </a> 
                        </div>
                        <!-- /Mobile Menu -->
                        <!-- Menu Start -->
                        <nav class="collapse navbar-collapse menu">
                            <ul class="nav navbar-nav sf-menu">
                                <li>
                                    <a id="current" href="index.jsp">
                                    	홈
                                    </a>
                                    
                                </li>
                                <li>
                                    <a href="search_room.jsp" class="sf-with-ul">
                             		       매물검색
                                  
                                    </a>
                                </li>
                                <li>
                                    <a href="register_room.jsp" class="sf-with-ul">
                                    매물 등록
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="sf-with-ul">
                                    마이페이지
                                    </a>
                                </li>
                              
                            </ul>
                        </nav>
                        <!-- /Menu --> 
                    </div>
                </div>
                <!-- /Main Header -->
            </header>
</body>
</html>