<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/search-room.css">
<title>Insert title here</title>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" charset='UTF-8' src="//apis.daum.net/maps/maps3.js?apikey=9091c7b57e5b32c1d21d9e0647a07843&libraries=services,clusterer"></script>
<script type="text/javascript" src="js/search_room.js"></script>
</head>
<body>
 <jsp:include page="header.jsp"></jsp:include>
 <section id="main">
 	<div style="margin-top:10px; padding-top:10px; margin-bottom:5px; padding-bottom:5px;"class="search-bar main-content">
 		<div class="container address-option">
 			<div class="row">
 				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 wow fadeIn">
            		<select id="sido-select" class="form-control input-md">
						<option value="서울시">서울시</option>
						<option value="경기도">경기도</option>
						<option value="인천시">인천시</option>
						<option value="부산시">부산시</option>
						<option value="대구시">대구시</option>
						<option value="대전시">대전시</option>
						<option value="울산시">울산시</option>
						<option value="세종시">세종시</option>
						<option value="광주시">광주시</option>
						<option value="강원도">강원도</option>
						<option value="충청북도">충청북도</option>
						<option value="충청남도">충청남도</option>
						<option value="경상북도">경상북도</option>
						<option value="경상남도">경상남도</option>
						<option value="전라북도">전라북도</option>
						<option value="전라남도">전라남도</option>
						<option value="제주도">제주도</option>
	       			</select>
	   			</div>
	           	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 wow fadeIn">
	            	<select id="sigungu-select" class="form-control input-md" >
	                        <option>전체</option>
					</select>
				</div>
	           	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 wow fadeIn">
	            	<select id="dong-select"class="form-control input-md">
                        <option>전체</option>
            		</select>
          		</div>
			</div>


 		</div>
 	</div>
	<div class="row">
		<div style="padding-right:0px;" class="col-lg-9 col-sm-9 col-md-9 col-xs-9">
			
			<div id="map">
				
			</div>	
		<div id="result"></div>
		</div> 
		<div style="padding-left:0px;" class="col-lg-3 col-sm-3 col-md-3 col-xs-3 item-list-container">
			<div id="item-list-container" class="list-group">
                <div class='detail-option-container'>
                    <div class='form-inline row'>
                        <div class='form-group col-lg-6 col-sm-6 col-md-6 col-xs-6 border-right'>
                            <p>보증금</p>
                            <input class='form-control-sm input-sm option-input' type='text' id='deposit-min' name='deposit-min' value='0'> ~ <input class='form-control-sm input-sm option-input' type='text' name='deposit-max' id='deposit-max' value='99999'>
                        </div>
                        <div class='form-group col-lg-6 col-sm-6 col-md-6 col-xs-6'>
                            <p>월세</p>
                            <input class='form-control-sm input-sm option-input' type='text' name='monthly-rent-min' id='monthly-rent-min' value='0'> ~ <input class='form-control-sm input-sm option-input' type='text' name='monthly-rent-max' id='monthly-rent-max' value='99999'>
                        </div>
                    </div>
                    <div class='form-inline row'>
                        <div class='form-group col-lg-12 col-sm-12 col-md-12 col-xs-12'>
                            <p>방 크기</p>
                            <input class='form-control-sm input-sm option-input' type='text' id='room-size-min' name='room-size-min' value='0'> ~ <input class='form-control-sm input-sm option-input' type='text' id='room-size-max' name='room-size-max' value='99999'>
                        </div>
                    </div>
                    <div class="item-type">
                        <h4>방구조</h4>
                        <label><input type="checkbox" name="room" class="item_room" value="원룸(오픈형)" checked="checked"> 원룸(오픈형)</label>
                        <label><input type="checkbox" name="room" class="item_room" value="원룸(분리형)" checked="checked"> 원룸(분리형)</label>
                        <label><input type="checkbox" name="room" class="item_room" value="원룸(복층형)" checked="checked"> 원룸(복층형)</label>
                        <label><input type="checkbox" name="room" class="item_room" value="투룸" checked="checked"> 투룸</label>
                        <label><input type="checkbox" name="room" class="item_room" value="쓰리룸" checked="checked"> 쓰리룸</label>
                    </div>
                </div>
                <div class='drop-down-container'>
                    <div class='image-wrapper'>
                        <a id='down-button'>
                            <i class='fa fa-angle-down'></i>
                        </a>
                    </div>
                </div>
				<div id="item-header">
					<h4 class="list-tit premium_special" style="display: block;">
					<em class="txt-premium">안심중개사</em> 이 지역 <strong>추천 방</strong> <span></span> <em class="zbIcon icon-question"></em></button></h4>
				</div>
			</div>
		</div>
		</div>
 </section>
 <jsp:include page="footer.jsp"></jsp:include>


</body>

</html>