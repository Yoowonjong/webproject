<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.SQLException"%>
<%@page import="org.json.JSONException"%>
<%@page import="DB.DAO"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="Log.Log"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%
		String line;
		StringBuffer jsonBuffer = new StringBuffer();
		request.setCharacterEncoding("utf-8");
		BufferedReader reader = request.getReader();
		while((line = reader.readLine()) != null)
		{
			jsonBuffer.append(line);
		}
		String json = jsonBuffer.toString();
		json = URLDecoder.decode(json, "UTF-8");
		Log.i("searchRoomList :: 인풋 Get :" + json);
		JSONObject jsonObject = new JSONObject(json);
		
		String sido = jsonObject.getString("sido");
		String sigungu = jsonObject.getString("sigungu");
		String dong = jsonObject.getString("dong");
		
		JSONObject sendData = new JSONObject();
		
		if(sido == null || sigungu == null || dong == null)
		{
			Log.d("sido, sigungu, dong 하나가 null!!");
			sendData.put("result", "false");
			sendData.put("reason", "JSONException");
		}
		
		 
		try{
			JSONArray items = DAO.getAddressList(sido, sigungu, dong);
			sendData.put("room_list", items);
			sendData.put("result", "true");
		} catch(JSONException e) {
			sendData.put("result", "false");
			sendData.put("reason", "JSONException");
			e.printStackTrace();
		} catch(SQLException e) {
			sendData.put("result", "false");
			sendData.put("reason", "SQLException");
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
		Log.d(sendData.toString());
		response.getWriter().write(sendData.toString());
	%>