<%@page import="com.sun.corba.se.impl.orbutil.ObjectWriter"%>
<%@page import="DB.RoomShortInfo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.SQLException"%>
<%@page import="org.json.JSONException"%>
<%@page import="DB.DAO"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="Log.Log"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String line;
	StringBuffer jsonBuffer = new StringBuffer();
	request.setCharacterEncoding("utf-8");
	BufferedReader reader = request.getReader();
	while((line = reader.readLine()) != null)
	{
		jsonBuffer.append(line);
	}
	String json = jsonBuffer.toString();
	json = URLDecoder.decode(json, "UTF-8");
	Log.i("searchRoomShortInfo:: 인풋 Get :" + json);
	JSONObject jsonObject = new JSONObject(json);
	
	JSONArray roomArray = jsonObject.getJSONArray("room_id");
	ArrayList<Integer> roomIDArray = new ArrayList<Integer>();
	
	for(int i = 0; i < roomArray.length(); i++)
	{
		int roomID = roomArray.getInt(i);
		roomIDArray.add(roomID);
	}
	
	JSONArray infoArray = DAO.getRoomShortInfo(roomIDArray);
	
	
	
	JSONObject resultData = new JSONObject();
	
	resultData.put("room_list", infoArray);

	//TODO : Result:값 넣기
	
	response.setContentType("application/json");
	Log.d(resultData.toString());
	response.getWriter().write(resultData.toString());
%>