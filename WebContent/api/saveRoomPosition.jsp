<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.SQLException"%>
<%@page import="org.json.JSONException"%>
<%@page import="DB.DAO"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="Log.Log"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String line;
	StringBuffer jsonBuffer = new StringBuffer();
	request.setCharacterEncoding("utf-8");
	BufferedReader reader = request.getReader();
	while((line = reader.readLine()) != null)
	{
		jsonBuffer.append(line);
	}
	String json = jsonBuffer.toString();
	json = URLDecoder.decode(json, "UTF-8");
	Log.i("searchRoomList :: 인풋 Get :" + json);
	JSONObject jsonObject = new JSONObject(json);
	
	int room_id = jsonObject.getInt("room_id");
	double latitude = jsonObject.getDouble("lat");
	double longitude = jsonObject.getDouble("lng");
	
	JSONObject jsonResult = new JSONObject();
	if(DAO.setAddressLatLng(room_id, latitude, longitude))
	{
		jsonResult.put("result", true);
		
	}
	else
	{
		jsonResult.put("result", false);
		jsonResult.put("reason", "데이터베이스 업데이트 실패");
	}
	response.setContentType("application/json");
	Log.d(jsonResult.toString());
	response.getWriter().write(jsonResult.toString());
%>