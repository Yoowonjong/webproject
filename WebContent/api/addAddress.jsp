<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.SQLException"%>
<%@page import="org.json.JSONException"%>
<%@page import="DB.DAO"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="Log.Log"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String line;
	StringBuffer jsonBuffer = new StringBuffer();
	request.setCharacterEncoding("utf-8");
	BufferedReader reader = request.getReader();
	while((line = reader.readLine()) != null)
	{
		jsonBuffer.append(line);
	}
	String json = jsonBuffer.toString();
	json = URLDecoder.decode(json, "UTF-8");
	Log.i("addAddress :: 인풋 Get :" + json);
	JSONObject jsonObject = new JSONObject(json);
	
	String sido = jsonObject.getString("sido");
	String sigungu= jsonObject.getString("sigungu");
	String dong = jsonObject.getString("dong");
	String jibun_address = jsonObject.getString("jibun_address");
	String road_address = jsonObject.getString("road_address");
	double latitude = jsonObject.getDouble("latitude");
	double longitude = jsonObject.getDouble("longitude");
	
	JSONObject jsonResult = new JSONObject();
	if(DAO.addAddress(sido, sigungu, dong, jibun_address, road_address, latitude, longitude))
	{
		jsonResult.put("result", "true");
	}
	
	response.setContentType("application/json");
	Log.d(jsonResult.toString());
	response.getWriter().write(jsonResult.toString());
%>