<%@page import="java.util.ArrayList"%>
<%@page import="org.json.JSONArray"%>
<%@page import="DB.DAO"%>
<%@page import="Log.Log"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	String line;
	StringBuffer jsonBuffer = new StringBuffer();
	request.setCharacterEncoding("utf-8");
	BufferedReader reader = request.getReader();
	while((line = reader.readLine()) != null)
	{
		jsonBuffer.append(line);
	}
	String json = jsonBuffer.toString();
	json = URLDecoder.decode(json, "UTF-8");
	Log.i("searchRoomList :: 인풋 Get :" + json);
	JSONObject jsonObject = new JSONObject(json);
	
	
	//왼쪽 위 좌표와 오른쪽 아래 좌표를 받아서 DB를 검색한다.
	double start_x = jsonObject.getDouble("start_x");
	double start_y = jsonObject.getDouble("start_y");
	double end_x = jsonObject.getDouble("end_x");
	double end_y = jsonObject.getDouble("end_y");
	
	int deposit_min = jsonObject.getInt("deposit_min");
	int deposit_max = jsonObject.getInt("deposit_max");
	
	int monthly_rent_min = jsonObject.getInt("monthly_rent_min");
	int monthly_rent_max = jsonObject.getInt("monthly_rent_max");
	
	int room_size_min = jsonObject.getInt("room_size_min");
	int room_size_max = jsonObject.getInt("room_size_max");
	
	JSONArray room_structure = jsonObject.getJSONArray("room_structure");
	ArrayList<String> room_array = new ArrayList<String>();
	
	for(int i = 0; i < room_structure.length(); i++)
	{
		String tmp = room_structure.getString(i);
		tmp = tmp.replaceAll("[)]", "\\)");
		tmp = tmp.replaceAll("[(]", "\\(");
		room_array.add(tmp);
	}
	
	Log.i("실행");
	JSONArray a = DAO.getRoomListWithPosition(
			start_x, start_y, 
			end_x, end_y,
			deposit_min, deposit_max,
			monthly_rent_min, monthly_rent_max,
			room_size_min, room_size_max,
			room_array
			);
	Log.i("실행");
	JSONObject resultData = new JSONObject();
	
	resultData.put("room_list", a);
	resultData.put("result", "true");
	response.setContentType("application/json");
	Log.d("검색된 매물 정보 : " + resultData.toString());
	response.getWriter().write(resultData.toString());
%>