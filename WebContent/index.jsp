<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  
<html> 
    <head>
    </head>
    <body class="home">
    <%
    	//URL을 가져온다. 로그인이나 로그아웃 할 시 그 페이지로 이동하기 위해서
    	request.setCharacterEncoding("utf-8");	
    	String url = request.getParameter("url");
    	
    %>
        <jsp:include page="header.jsp"></jsp:include>
            <!-- /Header --> 
            <!-- Main Section -->
            <section id="main">
                <!-- Slider -->
                <div class="fullwidthbanner-container tp-banner-container">
                    <div class="fullwidthbanner rslider tp-banner">
                        <ul>
                            <!-- THE FIRST SLIDE -->
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="homeslider_thumb2.jpg" data-delay="10000"  data-saveperformance="on">
                                <!-- MAIN IMAGE -->
                                <!-- LAYERS -->
                                <!-- LAYER NR. 1 -->
                                <iframe style="width:100%; height:100%; display:block;" data-fullwidthcentering="on"  src="home_vr_banner.html" data-lazyload="mg/redbg_big.png"></iframe>
                            </li>
                            <!-- /THE FIRST SLIDE -->
                            <!-- THE RESPONSIVE SLIDE -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="300">
                                <img src="img/slider/slider-bg-2.jpg" data-fullwidthcentering="on" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Slider --> 
                <!-- Main Content -->
                <div class="main-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeIn">
                                <div class="content-box big ch-item bottom-pad-small">
                                    <div class="ch-info-wrap">
                                        <div class="ch-info">
                                            <div class="ch-info-front ch-img-1"><i class="fa fa-rocket"></i></div>
                                            <div class="ch-info-back">
                                                <i class="fa fa-rocket"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-box-info">
                                        <h3>매물 등록</h3>
                                        <p>
                                           	 직거래를 원하는 방주인 혹은 공인중개사가 자신의 방에 대한 VR파노라마 사진과 정보를 등록하는 시스템.
                                        </p>
                                        <a href="#">더 보기 <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="border-bottom margin-top30">
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeIn">
                                <div class="content-box big ch-item bottom-pad-small">
                                    <div class="ch-info-wrap">
                                        <div class="ch-info">
                                            <div class="ch-info-front ch-img-1"><i class="fa fa-check"></i></div>
                                            <div class="ch-info-back">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-box-info">
                                        <h3>알림 기능</h3>
                                        <p>
                                           	 사용자가 원하는 방에 대한 정보가 없을때 사용자가 요청하면 요청한 자료가 업로드 될때 그에 대한 정보를 E-MAIL 혹은 문자메세지로 알려주는 시스템
                                        </p>
                                        <a href="#">더 보기 <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="border-bottom margin-top30">
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeIn">
                                <div class="content-box big ch-item">
                                    <div class="ch-info-wrap">
                                        <div class="ch-info">
                                            <div class="ch-info-front ch-img-1"><i class="fa fa-tags"></i></div>
                                            <div class="ch-info-back">
                                                <i class="fa fa-tags"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-box-info">
                                        <h3>안심 거래</h3>
                                        <p>
                                            	직거래를 하는 고객에 한해서 계약에 관련해서 안전한 거래를 위해 해당 지역에 위치하는 공인중개사 매칭
                                        </p>
                                        <a href="#">더 보기 <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="border-bottom margin-top30">
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-content">
                    <div class="container">
                    <h2 class="page-header">간편 매물 검색</h3>
                    	<form class=form-inline" role="form">
                    		<div class="form-group">
		                    	<div class="row">
		                    		<div class="col-sm-3 wow fadeIn">
				                    	<select class="form-control input-lg">
			                        		<option value="서울시">서울시</option>
			                        		<option value="경기도">경기도</option>
			                        		<option value="인천시">인천시</option>
			                        		<option value="부산시">부산시</option>
			                        		<option value="대구시">대구시</option>
			                        		<option value="대전시">대전시</option>
			                        		<option value="울산시">울산시</option>
			                        		<option value="세종시">세종시</option>
			                        		<option value="광주시">광주시</option>
			                        		<option value="강원도">강원도</option>
			                        		<option value="충청북도">충청북도</option>
			                        		<option value="충청남도">충청남도</option>
			                        		<option value="경상북도">경상북도</option>
			                        		<option value="경상남도">경상남도</option>
			                        		<option value="전라북도">전라북도</option>
			                        		<option value="전라남도">전라남도</option>
			                        		<option value="제주도">제주도</option>
		                        		</select>
		                    		</div>
			                    	<div class="col-sm-3 wow fadeIn">
				                    	<select class="form-control input-lg" >
				                    	
				                    	</select>
		                    		</div>
			                    	<div class="col-sm-3 wow fadeIn">
				                    	<select class="form-control input-lg">
				                    	
				                    	</select>
		                    		</div>
		                    		<div class="col-sm-3 wow fadeIn">
				                    	<button type="button" class="btn btn-block input-lg">검색</button>
			                    	</div>
		                    	</div>
	                    	</div>
                    	</form>
                    </div>
                </div>
            </section>
            <!-- /Main Section -->
            <!-- Footer -->
            <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>

