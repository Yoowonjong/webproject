<%@page import="Log.Log"%>
<%@page import="login.LoginData"%>
<%@page import="DB.DAO"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
	
	<%
		//기본 URL
		String url = "index.jsp";
		
		//해당 페이지로 돌아가기 위해서..
		String req = request.getHeader("REFERER");
	
		//POST로 로그인 처리 부분
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		if(email == null || password == null)
		{
			//Error페이지로 이동
		}
		
		request.setCharacterEncoding("utf-8");
		LoginData data = new LoginData(email, password);
		
		if(DAO.loginCheck(data))
		{
			Log.i("email : " + email + "--> 로그인 성공");
			//일단은 이메일로 세션 저장한다!!
			//향후 키값으로 바꾸든지 ㅋㅋ
			
			session.setAttribute("email", email);
			response.sendRedirect(req);
			//메인페이지로 이동
		}
		else
		{
			Log.i("email : " + email+ "--> 로그인 실패");
			//로그인 페이지로 이동
			response.sendRedirect(req);
			
		}
	%>
</body>
</html>