<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<%@page import="DB.DAO"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="java.io.*" %>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>

<%
	request.setCharacterEncoding("UTF-8");

	int security_deposit=Integer.parseInt(request.getParameter("security_deposit"));
	int monthly_rent=Integer.parseInt(request.getParameter("monthly_rent"));
	String room_structure=request.getParameter("room_structure");
	int management_expenses=Integer.parseInt(request.getParameter("management_expenses"));
	String area_for_exclusive_use=request.getParameter("area_for_exclusive_use");
	String area_for_agreement=request.getParameter("area_for_agreement");
	String number_of_building_story=request.getParameter("number_of_building_story");
	String number_of_story=request.getParameter("number_of_story");
	String building_dirtection=request.getParameter("building_direction");
	String is_available_loan=request.getParameter("is_available_loan");
	String is_available_animal=request.getParameter("is_available_animal");
	String is_available_parking=request.getParameter("is_available_parking");
	String is_available_elevator=request.getParameter("is_available_elevator");
	String sido = request.getParameter("sido");
	String sigungu = request.getParameter("sigungu");
	String dong = request.getParameter("dong");
	String address_number = request.getParameter("address-number");
	String jibun_address = request.getParameter("jibun-address");
	String road_address = request.getParameter("road-address");
	String remainder_address = request.getParameter("remainder_address");
	double latitude = Double.parseDouble(request.getParameter("lat"));
	double longitude = Double.parseDouble(request.getParameter("lng"));
	String newFileName = request.getParameter("uploadFile");
	int picture_type = Integer.parseInt(request.getParameter("picture_type"));
	
	String [] arr = request.getParameterValues("management_list");
	String [] arr2 = request.getParameterValues("room_options");


	String savePath = request.getRealPath("")+"/webproject/picture/"; 
	
	String uploadFile = "";
	int maxSize = 1024*1024*50;//50Mbyte 제한
	
    try{
	 
    MultipartRequest multi = new MultipartRequest(request, savePath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
     
    // 파일업로드
    uploadFile = multi.getFilesystemName("uploadFile");


	}catch(Exception e){
    	e.printStackTrace();
	}

	
	int i=0;
	String str="";
	String str2="";
	String comma=" ";

	for(i=0;i<arr.length; i++)
	{
		str=str+arr[i]+comma;
	}
	
	for(i=0;i<arr2.length; i++)
	{
		str2=str2+arr2[i]+comma;
	}

	String email=(String)session.getAttribute("email");
	
	int index = DAO.Select_id(email);
	int room_index=DAO.Select_room(index);
	
	DAO.room(index, security_deposit, monthly_rent, room_structure, management_expenses,  area_for_exclusive_use,  area_for_agreement, 
			 number_of_building_story,  number_of_story,  building_dirtection,  is_available_loan,  is_available_animal, is_available_parking, 
			 is_available_elevator, str,str2);
	
	DAO.address(room_index, sido, sigungu, dong, remainder_address,  jibun_address, road_address, latitude,  longitude,  address_number);
	DAO.room_picture(room_index, newFileName,picture_type);



%>
