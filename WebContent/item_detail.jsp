<%@page import="Log.Log"%>
<%@page import="DB.DAO"%>
<%@page import="item.ItemDetailInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/item_detail.css">
<link  href="img-viewer/viewer.min.css" rel="stylesheet">
<script src="img-viewer/viewer.min.js"></script>
<script type="text/javascript" charset='UTF-8' src="//apis.daum.net/maps/maps3.js?apikey=9091c7b57e5b32c1d21d9e0647a07843&libraries=services,clusterer"></script>

</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<%
		request.setCharacterEncoding("utf-8");
		int room_id = 0;
		try{
			room_id = Integer.parseInt(request.getParameter("room_id"));
			
		} catch(NumberFormatException e) {
			response.sendRedirect("error_404.jsp");
		}
		ItemDetailInfo info = DAO.getRoomDetail(room_id);
		if(info == null) Log.d("데이터 접근 실패");
	%>
	<div class="main-content">
    	<div class="container">
    		<div class='row'>
    			<div class='col-lg-9 col-sm-9 col-md-9 col-xs-9'>
					<div class='info-area panel panel-primary'>
						<div class='panel-heading'>
							<h2 class='panel-title' id='title'>매물 상세 정보</h3>
						</div>
						<div class='panel-body'>
							<div style='margin:5px; padding:3px;'>
								<div class='row info-div'>
									<div class='col-lg-6 col-sm-6 col-md-6 col-xs-6 info'>
										<label>일반 원룸</label>
										<label>안심 중개!</label>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h3>보증금/월세</h3>
										<p style='font-size:20pt'><%=info.getSecurity_deposit() %> / <%=info.getMonthly_rent() %><i style='font-size:10px'>만원</i></p> 
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h3>공급/전용면젹</h3>
										<p style='font-size:20pt'><%=info.getArea_for_exclusive_use() %> / <%=info.getArea_for_agreement() %> <i style='font-size:10px'>m^2</i></p>
									</div>
								</div>
								<div class='row info-div'>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>해당층/층층</h4>
										<p><%=info.getNumber_of_building_story() %> / <%=info.getNumber_of_story() %></p>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>관리비</h4>
										<%=info.getManagement_expenses() %>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>입주가능일</h4>
										<%=info.getAvailable_move_in_date() %>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>방 구조</h4>
										<%=info.getRoom_structure() %>
									</div>
								</div>
								<div class='row info-div'>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info' >
										<h4>해당층/총층</h4>
										<p><%=info.getNumber_of_building_story() %> / <%=info.getNumber_of_story() %></p>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>방향</h4>
										<%=info.getBuilding_dirtection() %>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info'>
										<h4>옵션</h4>
										<%=info.getOptions() %>
									</div>
									<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3 info' >
										<h4>주차여부</h4>
										<%= info.getIs_available_parking() %>
									</div>
								</div>
							</div>
							<div>
								<h3>제목</h3>
								<p><%=info.getTitle() %></p>
							</div>
							<div>
								<h3>상세설명</h3>
								<p><%=info.getComment()%></p>
							</div>
						</div>
					</div>
					<div class='panel panel-info'>
						<div class='panel-heading'>
							<h3 class='panel-title'>매물 일반 사진</h3>
						</div>
						<div class='panel-body'>
							<div class='image-viewer clearfix'>
							  <ul id="images">
						    	<li class='thumbnail'><img src="tintamarre.jpg" alt="Picture"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 2"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 3"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 4"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 5"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 6"></li>
							    <li class='thumbnail'><img src="tintamarre.jpg" alt="Picture 7"></li>
							  </ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-3 col-sm-3 col-md-3 col-xs-3'>
					<div class='panel panel-success'>
						<div class='panel-heading'>
							<h3 class='panel-title'>등록자 정보</h3>
						</div>
						<div class='panel-body'>
							<div class='row'>
								<div class='col-lg-6 col-sm-6 col-md-4 col-xs-4'>
									<img src='basic_profile.png'>
								</div>
								<div class='col-lg-6 col-sm-6 col-md-8 col-xs-8'>
									<% 
									if(info.getUser_type() == 2)
									{
										out.print("<h4>공인중개사</h4>");
									}
									else
									{
										out.print("<h4>직거래</h4>");
									}
									%>
									<%=info.getName() %><br>
								</div>
							</div>
							<h5>연락처</h5>
							<p><%=info.getPhone_number() %></p>
							<%
							if(info.getUser_type() == 2)
							{
							%>
							<h5>공인중개사 이름</h5>
							<p><%=info.getAgent_name()%></p>
							<h5>위치</h5>
							<p><%=info.getAddress() %></p>
							<h5>사무실 전화번호</h5>
							<p><%=info.getTelephone_number() %></p>
							<%
							}
							%>
						</div>
					</div>
				</div>
				
			</div>
			<div class='row'>
				<div id='map' class='col-lg-6 col-sm-6 col-md-6 col-xs-6' style="height:400px;">
					
				</div>
				<script>
					var mapContainer = document.getElementById('map');
					var mapOption = {
						center : new daum.maps.LatLng(<%=info.getLatitude()%>, <%=info.getLongitude()%>),
						level : 3
					};
					
					var map = new daum.maps.Map(mapContainer, mapOption);
				</script>
				<div id='load-map' class='col-lg-6 col-sm-6 col-md-6 col-xs-6' style="height:400px;">
					
				</div>
				<script>
				var roadviewContainer = document.getElementById('load-map'); //로드뷰를 표시할 div
				var roadview = new daum.maps.Roadview(roadviewContainer); //로드뷰 객체
				var roadviewClient = new daum.maps.RoadviewClient(); //좌표로부터 로드뷰 파노ID를 가져올 로드뷰 helper객체

				var position = new daum.maps.LatLng(<%=info.getLatitude()%>, <%=info.getLongitude()%>);
					roadviewClient.getNearestPanoId(position, 50, function(panoId) {
					    roadview.setPanoId(panoId, position); //panoId와 중심좌표를 통해 로드뷰 실행
					});
					
					var map2 = new daum.maps.Map(mapContainer2, mapOption2);
				</script>
			</div>
		</div>
	</div>
	
	<jsp:include page="footer.jsp"></jsp:include>
	<script src="js/item_detail.js"></script>
</body>
</html>