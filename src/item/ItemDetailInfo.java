package item;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

public class ItemDetailInfo {
	//일반 이미지 Path
	private ArrayList<String> normalImagePaths = new ArrayList<String>();
	//VR 이미지 Path
	private ArrayList<String> vrImagePaths = new ArrayList<String>();
	
	private int u_id;
	private int security_deposit;
	private Timestamp create_time;
	private int monthly_rent;
	private String room_structure;
	private int management_expenses;
	
	private String area_for_exclusive_use;
	private String area_for_agreement;
	private String number_of_building_story;
	private String number_of_story;
	private String building_dirtection;
	private String options;
	private String is_available_loan;
	private String is_available_animal;
	private String is_available_parking;
	private String is_available_elevator;
	private Date available_move_in_date;
	private String management_expenses_list;
	private String title;
	private String comment;
	
	private int user_type;
	
	
	private String name;
	private String phone_number;
	private String email;
	
	
	//agent 전용
	
	private String address;
	private String agent_name;
	private String telephone_number;
	
	
	//지도에서 띄어주는 데이터
	
	private double latitude;
	private double longitude;
	
	
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public ArrayList<String> getNormalImagePaths() {
		return normalImagePaths;
	}
	public void setNormalImagePaths(ArrayList<String> normalImagePaths) {
		this.normalImagePaths = normalImagePaths;
	}
	public ArrayList<String> getVrImagePaths() {
		return vrImagePaths;
	}
	public void setVrImagePaths(ArrayList<String> vrImagePaths) {
		this.vrImagePaths = vrImagePaths;
	}
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public int getSecurity_deposit() {
		return security_deposit;
	}
	public void setSecurity_deposit(int security_deposit) {
		this.security_deposit = security_deposit;
	}
	public Timestamp getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}
	public int getMonthly_rent() {
		return monthly_rent;
	}
	public void setMonthly_rent(int monthly_rent) {
		this.monthly_rent = monthly_rent;
	}
	public String getRoom_structure() {
		return room_structure;
	}
	public void setRoom_structure(String room_structure) {
		this.room_structure = room_structure;
	}
	public int getManagement_expenses() {
		return management_expenses;
	}
	public void setManagement_expenses(int management_expenses) {
		this.management_expenses = management_expenses;
	}
	public String getArea_for_exclusive_use() {
		return area_for_exclusive_use;
	}
	public void setArea_for_exclusive_use(String area_for_exclusive_use) {
		this.area_for_exclusive_use = area_for_exclusive_use;
	}
	public String getArea_for_agreement() {
		return area_for_agreement;
	}
	public void setArea_for_agreement(String area_for_agreement) {
		this.area_for_agreement = area_for_agreement;
	}
	public String getNumber_of_building_story() {
		return number_of_building_story;
	}
	public void setNumber_of_building_story(String number_of_building_story) {
		this.number_of_building_story = number_of_building_story;
	}
	public String getNumber_of_story() {
		return number_of_story;
	}
	public void setNumber_of_story(String number_of_story) {
		this.number_of_story = number_of_story;
	}
	public String getBuilding_dirtection() {
		return building_dirtection;
	}
	public void setBuilding_dirtection(String building_dirtection) {
		this.building_dirtection = building_dirtection;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getIs_available_loan() {
		return is_available_loan;
	}
	public void setIs_available_loan(String is_available_loan) {
		this.is_available_loan = is_available_loan;
	}
	public String getIs_available_animal() {
		return is_available_animal;
	}
	public void setIs_available_animal(String is_available_animal) {
		this.is_available_animal = is_available_animal;
	}
	public String getIs_available_parking() {
		return is_available_parking;
	}
	public void setIs_available_parking(String is_available_parking) {
		this.is_available_parking = is_available_parking;
	}
	public String getIs_available_elevator() {
		return is_available_elevator;
	}
	public void setIs_available_elevator(String is_available_elevator) {
		this.is_available_elevator = is_available_elevator;
	}
	public Date getAvailable_move_in_date() {
		return available_move_in_date;
	}
	public void setAvailable_move_in_date(Date available_move_in_date) {
		this.available_move_in_date = available_move_in_date;
	}
	public String getManagement_expenses_list() {
		return management_expenses_list;
	}
	public void setManagement_expenses_list(String management_expenses_list) {
		this.management_expenses_list = management_expenses_list;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getUser_type() {
		return user_type;
	}
	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getTelephone_number() {
		return telephone_number;
	}
	public void setTelephone_number(String telephone_number) {
		this.telephone_number = telephone_number;
	}
	
	//올린사람 판단하기!
	
	
	
}
