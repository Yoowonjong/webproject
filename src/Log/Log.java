package Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
	
	public static String getNowTime()
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy년 MM월 dd일  aa hh:mm:ss");
		
		String str = format.format(cal.getTime());
		return str;
	}
	
	public static void e(String msg)
	{
		System.out.println("[ERROR] " + getNowTime() + " " + msg);
	}
	public static void d(String msg)
	{
		System.out.println("[DEBUG] " + getNowTime() + " " + msg);
	}
	public static void i(String msg)
	{
		System.out.println("[INFO] " + getNowTime() + " " + msg);
	}
}
