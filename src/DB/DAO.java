package DB;

import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import Log.Log;
import item.ItemDetailInfo;
import login.LoginData;

public class DAO {
	
	public static final String ImageServerPath = "http://210.117.182.154:8080/";
	
	public static boolean loginCheck(LoginData data)
	{
		String email = data.getEmail();
		String pw = data.getPassword();
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		String SQL = "SELECT * FROM user WHERE email = \"" + email + "\" and password = HEX(AES_ENCRYPT(\"" + pw + "\",\"mykey\")) LIMIT 1";
		System.out.println(SQL);
		
		if(conn == null) System.out.println("conn NULL!!");
		//질의 
		try {
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(SQL);
			if(rs.next())
			{
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//return null --> 연결 실패
	
	public static JSONArray getAddressList(String sido, String sigungu, String dong) throws SQLException, JSONException
	{
		String SQL = "SELECT * FROM address WHERE sido=\'" + sido +"\' AND sigungu=\'" + sigungu + "\' AND dong=\'" + dong + "\';";
		//String SQL = "SELECT * FROM address";
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		
		
		System.out.println(SQL);
		
		
		JSONArray items = new JSONArray();
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return null;
		}
		
	
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(SQL);
		
		Log.d(""+rs.getFetchSize());
		//위도 경도가 -9999이면 없다는 뜻임.
		//자바스크립트 단에서 처리할 것.
		while(rs.next())
		{
			JSONObject item = new JSONObject();
			
			item.put("room_id", rs.getInt("room_id"));
			item.put("sido", rs.getString("sido"));
			item.put("sigungu", rs.getString("sigungu"));
			item.put("jibun_address", rs.getString("jibun_address"));
			item.put("road_address", rs.getString("road_address"));
			
			//위도
			double latitude = rs.getDouble("latitude");
			double logtitude = rs.getDouble("longitude");
			item.put("latitude", latitude);
			item.put("longitude", logtitude);
			Log.d(item.toString());
			//경도
			items.put(item);
		}
		Log.d(items.toString());
		
		stmt.close();
		conn.close();
		return items;
	}
	
	public static boolean setAddressLatLng(int room_id, double latitude, double longitude)
	{
		String SQL = "UPDATE address SET latitude = " + latitude + " , longitude = " + longitude + " WHERE room_id = " + room_id;
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		
		
		System.out.println(SQL);
		
		
		JSONArray items = new JSONArray();
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		
		ResultSet rs = null;
		Statement stmt = null;
		int resultCount = 0;
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static boolean addAddress(String sido, String sigungu, String dong, String jibun_address, String road_address, double latitude, double longitude)
	{
		String SQL = "INSERT INTO address (sido, sigungu, dong, jibun_address, road_address, latitude, longitude) "
				+ "VALUES (\"" + sido + "\", \"" + sigungu + "\", \"" + dong + "\", \"" + jibun_address + "\", \"" + road_address + "\", " + latitude + "," + longitude + ")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		
		
		System.out.println(SQL);
		
		
		JSONArray items = new JSONArray();
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	
	public static JSONArray getRoomListWithPosition(
			double start_x, double start_y,
			double end_x, double end_y,
			int deposit_min, int deposit_max,
			int monthly_rent_min, int monthly_rent_max,
			int room_size_min, int room_size_max,
			ArrayList<String> room_structure
			)
	{
		JSONArray infoArray = new JSONArray();
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			String SQL = "SELECT * FROM address NATURAL JOIN room WHERE " +
					"" +
					"address.latitude < " + end_x + " and " +
					"address.latitude > " + start_x +" and " +
					"address.longitude > " + start_y + " and " +
					"address.longitude < "+ end_y + " and "
					+ "room.security_deposit >= " + deposit_min + " and room.security_deposit <= " + deposit_max +" and"
					+ " room.monthly_rent >= "+ monthly_rent_min + " and room.monthly_rent <= " + monthly_rent_max + " and"
					+ " room.area_for_exclusive_use >= " + room_size_min + " and room.area_for_exclusive_use <= "+ room_size_max;
			if(room_structure.size() != 0)
			{
				SQL += " and (";
			}
			for(int i = 0; i < room_structure.size(); i++)
			{
				if(i == room_structure.size() - 1)
				{
					SQL += "room.room_structure = \"" + room_structure.get(i) + "\")";
					break;
				}
				else
				{
					SQL += "room.room_structure = \"" + room_structure.get(i) + "\" OR ";
				}
			}
			Log.d(SQL);
			
			rs = stmt.executeQuery(SQL);
			//System.out.println(SQL);
			ArrayList<JSONObject> tmp = new ArrayList<JSONObject>();
			while(rs.next())
			{
				JSONObject info = new JSONObject();
				int securityDeposit = rs.getInt("security_deposit");
				int monthlyRent = rs.getInt("monthly_rent");
				String roomStructure = rs.getString("room_structure");
				String title = rs.getString("title");
				int roomID = rs.getInt("room_id");
				double latitude = rs.getDouble("latitude");
				double longitude = rs.getDouble("longitude");
				info.put("room_id", roomID);
				info.put("security_deposit", securityDeposit);
				info.put("monthlyRent", monthlyRent);
				info.put("room_structure", roomStructure);
				info.put("title", title);
				info.put("latitude", latitude);
				info.put("longitude", longitude);
				tmp.add(info);
			}
			rs.close();
			//사진 1개를 가져오는 SQL
			for(int i = 0; i < tmp.size(); i++)
			{
				JSONObject now = tmp.get(i);
				String pictureSQL = "SELECT * FROM room_picture WHERE room_id = " + now.getInt("room_id") + " LIMIT 1";
				Log.d(SQL);
				ResultSet rs2 = stmt.executeQuery(pictureSQL);
				
				while(rs2.next())
				{
					String path = rs2.getString("path");
					String name = rs2.getString("picture_name");
					String filePath = DAO.ImageServerPath + path + name;
					now.put("picture_path", filePath);
				}
				infoArray.put(now);
				rs2.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		return infoArray;
	}
	public static JSONArray getRoomShortInfo(ArrayList<Integer> roomIDArray)
	{
		
		JSONArray infoArray = new JSONArray();
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			for(int i = 0; i < roomIDArray.size(); i++)
			{
				String SQL = "SELECT * FROM room WHERE room_id = " + roomIDArray.get(i);
				JSONObject info = new JSONObject();
				rs = stmt.executeQuery(SQL);
				System.out.println(SQL);
				while(rs.next())
				{
					int securityDeposit = rs.getInt("security_deposit");
					int monthlyRent = rs.getInt("monthly_rent");
					String roomStructure = rs.getString("room_structure");
					String title = rs.getString("title");
					info.put("room_id", roomIDArray.get(i));
					info.put("security_deposit", securityDeposit);
					info.put("monthlyRent", monthlyRent);
					info.put("room_structure", roomStructure);
					info.put("title", title);
				}
				rs.close();
				//사진 1개를 가져오는 SQL
				String pictureSQL = "SELECT * FROM room_picture WHERE room_id = " + roomIDArray.get(i) + " LIMIT 1";
				Log.d(SQL);
				ResultSet rs2 = stmt.executeQuery(pictureSQL);
		
				while(rs2.next())
				{
					String path = rs2.getString("path");
					String name = rs2.getString("picture_name");
					String filePath = DAO.ImageServerPath + path + name;
					info.put("picture_path", filePath);
					infoArray.put(info);
				}
				
				
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return infoArray;
		
	}
	public static boolean room(int u_id,int security_deposit, int monthly_rent, String room_structure, int management_expenses, String area_for_exclusive_use, String area_for_agreement, 
			String number_of_building_story, String number_of_story, String building_dirtection, String is_available_loan, String is_available_animal, String is_available_parking,
			String is_available_elevator, String str, String str2 )
	{
		String SQL = "INSERT INTO room (u_id, security_deposit, monthly_rent, room_structure, management_expenses,  area_for_exclusive_use,  area_for_agreement, number_of_building_story, "
				+ " number_of_story,  building_dirtection,  is_available_loan,  is_available_animal, is_available_parking,is_available_elevator,management_expenses_list,options) "
				+ "VALUES ("+ u_id + ", "+ security_deposit+ ", "+ monthly_rent +",  \"" + room_structure + "\", " + management_expenses + ", \"" + area_for_agreement + "\", \"" + area_for_agreement + 
				"\", \"" +  number_of_building_story + "\",\"" +  number_of_story + "\", \"" +  building_dirtection + "\",\"" +  is_available_loan + "\", \"" +  is_available_animal + "\", \"" +  
				is_available_parking + "\", \"" +  is_available_elevator + "\",\"" +  str + "\",\"" +  str2 + "\" )";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	
	public static int Select_id(String email) throws SQLException
	{
		String SQL = "SELECT u_id FROM user WHERE user.email = " + "email";
		int index = 0;
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
	
		System.out.println(SQL);
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return 0;
		}

		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(SQL);
		
		while(rs.next())
		{	
			index = rs.getInt("u_id");
		}
		
		stmt.close();
		conn.close();
		return index;
	}
	public static boolean address(int room_id, String sido, String sigungu, String dong, String remainder_address, String jibun_address,
			String road_address, double latitude, double longitude, String address_number)
	{
		String SQL = "INSERT INTO address ( room_id, sido, sigungu, dong,  remainder_address, jibun_address,  road_address, latitude, longitude,  address_number) "
				+ "VALUES ("+room_id+", \""+ sido+"\", \""+ sigungu +"\",  \"" + dong + "\", \"" + remainder_address + "\", \"" + jibun_address
				+ "\", \"" + road_address + "\", " +  latitude + "," +  longitude + ", \"" +  address_number + "\" )";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static int Select_room(int u_id) throws SQLException
	{
		String SQL = "SELECT room_id FROM room WHERE room.u_id = " + u_id;
		int index = 0;
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
	
		System.out.println(SQL);
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return 0;
		}

		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(SQL);
		
		while(rs.next())
		{	
			index = rs.getInt("room_id");
		}
		
		stmt.close();
		conn.close();
		return index;
	}
	public static boolean room_picture(int room_id, String picture_name, int picture_type)
	{
		String SQL = "INSERT INTO room_picture ( room_id, picture_name, picture_type) VALUES ("+room_id+", \""+ picture_name +"\","+picture_type+")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static boolean user(String email, String name, String password, String phone_number, int type)
	{
		String SQL = "INSERT INTO user( email, name, password, phone_number,user_type)"
				+ "VALUES (\""+email+"\", \""+ name +"\","+"HEX(AES_ENCRYPT(\"" + password+"\", \"mykey\")), \""+ phone_number + "\","+type+")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static boolean customer(int u_id)
	{
		String SQL = "INSERT INTO customer( u_id)VALUES ("+u_id +")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static boolean director(int u_id)
	{
		String SQL = "INSERT INTO director( u_id)VALUES ("+u_id +")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static boolean agent(int u_id, String address, String agent_name, String telephone_number)
	{
		String SQL = "INSERT INTO agent( u_id,address,agent_name,telephone_number)VALUES ("+u_id +",\""+address+"\",\""+agent_name+"\",\""+telephone_number+"\")";
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		
		System.out.println(SQL);
		
		
		if(conn == null)
		{
			System.out.println("conn NULL!!");
			return false;
		}
		Statement stmt = null;
		int resultCount = 0;
		
		try {
			stmt = conn.createStatement();
			resultCount = stmt.executeUpdate(SQL);
			System.out.println(resultCount);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		
		return resultCount > 0 ? true :false;
	}
	public static ItemDetailInfo getRoomDetail(int room_id)
	{
		
		DBConnector DB = new DBConnector();
		Connection conn = DB.makeConnection();
		ItemDetailInfo detail_info = new ItemDetailInfo();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			
				String SQL = "SELECT * FROM room WHERE room_id = " + room_id;
				rs = stmt.executeQuery(SQL);
				System.out.println(SQL);
				
				while(rs.next())
				{
					int u_id = rs.getInt("u_id");
					int security_deposit = rs.getInt("security_deposit");
					int monthly_rent = rs.getInt("monthly_rent");
					String room_structure = rs.getString("room_structure");
					int managemnt_expenses = rs.getInt("management_expenses");
					String area_for_exclusive_use = rs.getString("area_for_exclusive_use");
					String area_for_agreement = rs.getString("area_for_agreement");
					String number_of_building_story = rs.getString("number_of_building_story");
					String number_of_story = rs.getString("number_of_story");
					String building_dirtection = rs.getString("building_dirtection");
					String options = rs.getString("options");
					String is_available_loan = rs.getString("is_available_loan");
					String is_available_animal = rs.getString("is_available_animal");
					String is_available_parking = rs.getString("is_available_parking");
					String is_available_elevator = rs.getString("is_available_elevator");
					java.sql.Date available_move_in_date = rs.getDate("available_move_in_date");
					String management_expenses_list = rs.getString("management_expenses_list");
					String title = rs.getString("title");
					String comment = rs.getString("comment");
					detail_info.setU_id(u_id);
					detail_info.setSecurity_deposit(security_deposit);
					detail_info.setMonthly_rent(monthly_rent);
					detail_info.setRoom_structure(room_structure);
					detail_info.setManagement_expenses(managemnt_expenses);
					detail_info.setArea_for_exclusive_use(area_for_exclusive_use);
					detail_info.setArea_for_agreement(area_for_agreement);
					detail_info.setNumber_of_building_story(number_of_building_story);
					detail_info.setNumber_of_story(number_of_story);
					detail_info.setBuilding_dirtection(building_dirtection);
					detail_info.setOptions(options);
					detail_info.setIs_available_loan(is_available_loan);
					detail_info.setIs_available_animal(is_available_animal);
					detail_info.setIs_available_parking(is_available_parking);
					detail_info.setIs_available_elevator(is_available_elevator);
					detail_info.setAvailable_move_in_date(available_move_in_date);
					detail_info.setManagement_expenses_list(management_expenses_list);
					detail_info.setTitle(title);
					detail_info.setComment(comment);
					
					
				}
				rs.close();
				//일반 사진을 불러온다.
				ArrayList<String> normal_picture_path = new ArrayList<String>();
				ArrayList<String> vr_path = new ArrayList<String>();
				String pictureSQL = "SELECT * FROM room_picture WHERE room_id = " + room_id + " and picture_type=\"Normal\"";
				Log.d(SQL);
				ResultSet rs2 = stmt.executeQuery(pictureSQL);
				while(rs2.next())
				{
					String path = rs2.getString("path");
					String picture_name = rs2.getString("picture_name");
					//Todo : Path에 맞게 수정할 것
					normal_picture_path.add(path + picture_name);
				}
				detail_info.setNormalImagePaths(normal_picture_path);
				rs2.close();
				String VRSQL = "SELECT * FROM room_picture WHERE room_id = " + room_id + " and picture_type=\"Normal\"";
				ResultSet rs3 = stmt.executeQuery(VRSQL);
				
				while(rs3.next())
				{
					String path = rs3.getString("path");
					String picture_name = rs3.getString("picture_name");
					vr_path.add(path + picture_name);
				}
				detail_info.setVrImagePaths(vr_path);
				rs3.close();
				
				//유저 정보를 가져온다.
				String UserSQL = "SELECT * FROM user WHERE u_id = " + detail_info.getU_id() + " LIMIT 1";
				ResultSet rs4 = stmt.executeQuery(UserSQL);
				
				while(rs4.next())
				{
					int user_type = rs4.getInt("user_type");
					String email = rs4.getString("email");
					String phone_number = rs4.getString("phone_number");
					String name = rs4.getString("name");
					detail_info.setUser_type(user_type);
					detail_info.setEmail(email);
					detail_info.setPhone_number(phone_number);
					detail_info.setName(name);
				}
				rs4.close();
				
				//agent이면 해당 정보를 더 가져온다.
				if(detail_info.getUser_type() == 2)
				{
					String agentSQL = "SELECT * FROM agent WHERE u_id = " + detail_info.getU_id() + " LIMIT 1";
					ResultSet rs5 = stmt.executeQuery(agentSQL);
					while(rs5.next())
					{
						String address = rs5.getString("address");
						String agent_name = rs5.getString("agent_name");
						String telephone_number = rs5.getString("telephone_number");
						detail_info.setAddress(address);
						detail_info.setAgent_name(agent_name);
						detail_info.setTelephone_number(telephone_number);
					}
					rs5.close();
				}
				
				String PositionSQL = "SELECT * FROM address WHERE room_id = " + room_id;
				ResultSet rs6 = stmt.executeQuery(PositionSQL);
				while(rs6.next())
				{
					double longitude = rs6.getDouble("longitude");
					double latitude = rs6.getDouble("latitude");
				
					detail_info.setLatitude(latitude);
					detail_info.setLongitude(longitude);
					
				}
				
				rs6.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			 if (stmt != null) try {stmt.close();} catch(SQLException ex){}
			 if (conn != null) try {conn.close();} catch(SQLException ex){}
		}
		return detail_info;
	}
}
