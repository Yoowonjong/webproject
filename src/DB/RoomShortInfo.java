package DB;

public class RoomShortInfo {
	private int roomID;
	private int securityDeposit; //보증금
	private int monthlyRent; // 월세
	private String roomStructure; //방 구조(원룸)
	private String title; //title
	
	
	public RoomShortInfo(int roomID, int securityDeposit, int monthlyRent, String roomStructure, String title) {
		super();
		this.roomID = roomID;
		this.securityDeposit = securityDeposit;
		this.monthlyRent = monthlyRent;
		this.roomStructure = roomStructure;
		this.title = title;
	}
	
	
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	public int getSecurityDeposit() {
		return securityDeposit;
	}
	public void setSecurityDeposit(int securityDeposit) {
		this.securityDeposit = securityDeposit;
	}
	public int getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(int monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	public String getRoomStructure() {
		return roomStructure;
	}
	public void setRoomStructure(String roomStructure) {
		this.roomStructure = roomStructure;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
}
